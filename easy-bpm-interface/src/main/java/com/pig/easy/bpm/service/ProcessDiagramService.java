package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.response.ProcessDiagramDTO;
import com.pig.easy.bpm.utils.Result;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/7/4 14:16
 */
public interface ProcessDiagramService {

    Result<ProcessDiagramDTO> getProcessDiagramByApplyId(Long applyId);

    Result<ProcessDiagramDTO> getProcessDiagramByProInstId(String procInstId);
}
