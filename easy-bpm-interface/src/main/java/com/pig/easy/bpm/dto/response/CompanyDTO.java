package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/6 17:21
 */
@Data
@ToString
public class CompanyDTO extends BaseResponseDTO {

    private static final long serialVersionUID = -1881629035101609978L;

    private Long companyId;

    private String companyCode;

    private Long companyParentId;

    private String companyParentCode;

    private String companyName;

    private String companyAbbr;

    private Integer companyLevel;

    private Integer companyOrder;

    private String companyIcon;

    private String companyUrl;

    private String tenantId;

    private Integer companyStatus;

    private Long operatorId;

    private String operatorName;

}
