package com.pig.easy.bpm.mapper;

import com.pig.easy.bpm.entity.RoleGroupDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色组 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@Mapper
public interface RoleGroupMapper extends BaseMapper<RoleGroupDO> {

}
