package com.pig.easy.bpm;

import com.pig.easy.bpm.entity.NodeDO;
import com.pig.easy.bpm.utils.FlowElementUtils;
import org.flowable.bpmn.converter.BpmnXMLConverter;
import org.flowable.bpmn.model.*;
import org.flowable.bpmn.model.Process;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/13 14:41
 */
public class bpmModelTest {

    public static void main(String[] args) throws XMLStreamException {

        String xml =
                "  <definitions " +
                        "  xmlns='http://www.omg.org/spec/BPMN/20100524/MODEL' " +
                        "       xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                        " xmlns:bpmndi='http://www.omg.org/spec/BPMN/20100524/DI' " +
                        " xmlns:omgdc='http://www.omg.org/spec/DD/20100524/DC' " +
                        " xmlns:omgdi='http://www.omg.org/spec/DD/20100524/DI' " +
                        " xmlns:flowable='http://flowable.org/bpmn' " +
                        " xmlns:xsd='http://www.w3.org/2001/XMLSchema' " +
                        " targetNamespace='http://www.flowable.org/processdef'> " +
                        "<process id='pig:test1' name='测试流程1' isExecutable='true'>  " +
                        "        <documentation>测试流程1</documentation>  " +
                        "        <extensionElements>  " +
                        "          <flowable:customProperties selectPath=\"0\" handlerStrategy=\"skip\" isSequential=\"parallel\" proportion=\"100\" taskType=\"approve\" actionList=\"approve,return\" findUserType=\"1\" />  " +
                        "            <flowable:executionListener event='start' delegateExpression='#wss1' />  " +
                        "            <flowable:executionListener event='end' delegateExpression='#wss1' />  " +
                        "        </extensionElements>  " +
                        "        <startEvent id='StartEvent_1' name='开始节点' />" +
                        "   <userTask id=\"UserTask_07fx7np\" name=\"发起人\" flowable:priority=\"2\">\n" +
                        "            <extensionElements>\n" +
                        "                <flowable:customProperties selectPath=\"0\" handlerStrategy=\"skip\" isSequential=\"parallel\" proportion=\"100\" taskType=\"approve\" actionList=\"approve,return\" findUserType=\"1\" />\n" +
                        "            </extensionElements>\n" +
                        "        </userTask> " +
                        "   </process> " +
                        "      </definitions> ";

        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] bytes = xml.getBytes();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        XMLInputFactory xif = XMLInputFactory.newInstance();
        InputStreamReader in = null;
        try {
            in = new InputStreamReader(inputStream, "UTF-8");
            XMLStreamReader xtr = xif.createXMLStreamReader(in);
            BpmnModel bpmnModel = bpmnXMLConverter.convertToBpmnModel(xtr);

            List<NodeDO> nodeDOList = FlowElementUtils.bpmnModelToNodeDOList(bpmnModel);
            System.out.println("nodeDOList = " + nodeDOList);

            Process process = bpmnModel.getProcesses().get(0);

            Collection<FlowElement> flowElements = process.getFlowElements();
            for (FlowElement flowElement : flowElements){

                ExtensionElement extensionElement = FlowElementUtils.getExtensionElementFromFlowElementByName(flowElement, null);
                String selectPath = FlowElementUtils.getAttributesFromExtensionElementByName(extensionElement, "selectPath");

                System.out.println("selectPath = " + selectPath);
//                System.out.println("flowElement = " + flowElement.getName());
//                System.out.println("flowElement = " + flowElement.getId());
//                Map<String, List<ExtensionElement>> extensionElements = flowElement.getExtensionElements();
//                for (Map.Entry<String, List<ExtensionElement>> stringEntry : extensionElements.entrySet()){
//
//                    if(!stringEntry.getKey().equals("customProperties")){
//                        continue;
//                    }
//                    System.out.println("stringEntry.getKey() = " + stringEntry.getKey());
//                    System.out.println("stringEntry.getValue() = " + stringEntry.getValue());
//                    for(ExtensionElement extensionElement : stringEntry.getValue()){
//                        if(!extensionElement.getName().equals("customProperties")){
//                            continue;
//                        }
//                        Map<String, List<ExtensionAttribute>> stringListMap = extensionElement.getAttributes();
//                        for (Map.Entry<String, List<ExtensionAttribute>> listEntry: stringListMap.entrySet()) {
//                            System.out.println("listEntry.getKey() = " + listEntry.getKey());
//                            System.out.println("listEntry.getValue() = " + listEntry.getValue());
//                        }
//                        System.out.println("extensionElement = " + extensionElement);
//                    }
//                }
            }


            System.out.println("bpmnModel = " + bpmnModel);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }
}
