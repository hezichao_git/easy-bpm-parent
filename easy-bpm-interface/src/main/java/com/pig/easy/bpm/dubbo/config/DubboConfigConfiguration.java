package com.pig.easy.bpm.dubbo.config;/**
 * Created by Administrator on 2020/4/14.
 */

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.apache.dubbo.config.*;
import org.springframework.context.annotation.Bean;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/4/14 14:42
 */
public class DubboConfigConfiguration {

    @NacosValue(value = "${bpm.dubbo.server.name:}", autoRefreshed = true)
    private String serverName;

    @NacosValue(value = "${bpm.dubbo.registry.address:}", autoRefreshed = true)
    private String registryAddress;

    @NacosValue(value = "${bpm.dubbo.server.port:}", autoRefreshed = true)
    private String port;

    @NacosValue(value = "${bpm.dubbo.server.timeout:}", autoRefreshed = true)
    private String timeout;

    @Bean
    public ApplicationConfig applicationConfig() {
        this.checkData();
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName(serverName);
        applicationConfig.setQosEnable(false);
        applicationConfig.setQosAcceptForeignIp(true);
        applicationConfig.setQosPort(Integer.valueOf(port));
        applicationConfig.setId(serverName);

        System.out.println("#############################1########## = " + applicationConfig);
        return applicationConfig;
    }

    @Bean
    public ProtocolConfig protocolConfig() {
        this.checkData();
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setPort(Integer.valueOf(port));
        protocolConfig.setName("dubbo");
        protocolConfig.setHost("localhost");
        return protocolConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        this.checkData();

        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(registryAddress);
        registryConfig.setRegister(true);
        System.out.println("registryAddress = " + registryAddress);
        return registryConfig;
    }

    @Bean
    public ProviderConfig providerConfig() {
        this.checkData();
        ProviderConfig providerConfig = new ProviderConfig();
        providerConfig.setTimeout(NumberUtils.parseNumber(timeout,Integer.class));
        providerConfig.setRetries(0);

        return providerConfig;
    }

//    @Bean
//    public ExceptionFilter exceptionFilter(){
//        return new DubboExceptionFilter();
//    }

    @Bean
    public ConsumerConfig consumerConfig() {
        this.checkData();
        ConsumerConfig consumerConfig = new ConsumerConfig();
        consumerConfig.setTimeout(NumberUtils.parseNumber(timeout,Integer.class));
        consumerConfig.setRetries(0);
        consumerConfig.setCheck(false);
        return consumerConfig;
    }

    /**
     * 校验数据
     */
    private void checkData() {
        if (StringUtils.isEmpty(serverName)
                || StringUtils.isEmpty(port)
                || StringUtils.isEmpty(registryAddress)
                || NumberUtils.parseNumber(timeout,Integer.class) < 0) {
            throw new RuntimeException("dubboConfiguration init fail, please config on nacos");
        }
        System.out.println("dubbo name = " + serverName);
        System.out.println("dubbo registryAddress = " + registryAddress);

    }

    private RegistryConfig createRegistryConfig(String registryAddress, Boolean register) {
        this.checkData();
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(registryAddress);
        registryConfig.setRegister(register);

        return registryConfig;
    }


}
