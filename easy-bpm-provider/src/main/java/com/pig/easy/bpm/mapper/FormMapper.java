package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.dto.response.FormDTO;
import com.pig.easy.bpm.entity.FormDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@Mapper
public interface FormMapper extends BaseMapper<FormDO> {

    List<FormDTO> getListByCondition(FormDO form);

    Integer updateForm(FormDO form);
}
