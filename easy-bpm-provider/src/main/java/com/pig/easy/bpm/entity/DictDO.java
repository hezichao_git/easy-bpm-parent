package com.pig.easy.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("bpm_dict")
public class DictDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "dict_id", type = IdType.AUTO)
    private Long dictId;
    /**
     * 字典编码
     */
    @TableField("dict_code")
    private String dictCode;
    /**
     * 字典名称
     */
    @TableField("dict_name")
    private String dictName;
    /**
     * 租户编号
     */
    @TableField("tenant_id")
    private String tenantId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 状态 1 有效 0 失效
     */
    @TableField("valid_state")
    private Integer validState;
    /**
     * 操作人工号
     */
    @TableField("operator_id")
    private Long operatorId;
    /**
     * 操作人姓名
     */
    @TableField("operator_name")
    private String operatorName;

    @TableField("update_time")
    private LocalDateTime updateTime;

    @TableField("create_time")
    private LocalDateTime createTime;


}
