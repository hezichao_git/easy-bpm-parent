package com.pig.easy.bpm.mapper;

import com.pig.easy.bpm.dto.response.NodeInfoDTO;
import com.pig.easy.bpm.entity.NodeDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 流程节点表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-06-22
 */
public interface NodeMapper extends BaseMapper<NodeDO> {

    int batchInsert(@Param("list") List<NodeDO> addList);

    int batchUpdate(@Param("list") List<NodeDO> addList);

    List<NodeInfoDTO> getListByCondition(NodeDO nodeDO);
}
