package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.entity.ConfigDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-21
 */
@Mapper
public interface ConfigMapper extends BaseMapper<ConfigDO> {

    @Select("SELECT  " +
            "	`config_id`,  " +
            "	`config_code`,  " +
            "	`config_name`,  " +
            "	`template_id`,  " +
            "	`config_key`,  " +
            "	`config_value`,  " +
            "	`config_type`,  " +
            "	`tenant_id`,  " +
            "	`remarks`,  " +
            "	`valid_state`,  " +
            "	`operator_id`,  " +
            "	`operator_name`,  " +
            "	`create_time`,  " +
            "	`update_time`  " +
            "FROM  " +
            "	bpm_config t  " +
            "WHERE  " +
            "	t.config_key = #{tenantId}  " +
            "AND t.tenant_id = #{configKey}  " +
            "AND t.valid_state = 1  " +
            "LIMIT 1")
    ConfigDO getConfigValue(@Param("tenantId") String tenantId, @Param("configKey") String configKey);
}
