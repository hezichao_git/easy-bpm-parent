package com.pig.easy.bpm.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.pig.easy.bpm.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-21
 */
@RestController
@RequestMapping("/configTemplate")
public class ConfigTemplateController extends BaseController {

}

