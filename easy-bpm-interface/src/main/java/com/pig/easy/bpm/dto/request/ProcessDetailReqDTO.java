package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/6 12:59
 */
@Data
@ToString
public class ProcessDetailReqDTO extends BaseRequestDTO {

    private static final long serialVersionUID = -6016522231039864019L;

    private Long processDetailId;

    private String tenantId;

    private Long processId;

    private String processXml;

    private String applyTitleRule;

    private Date applyDueDate;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private Integer pageIndex;

    private Integer pageSize;

    private Integer publishStatus;

    private Integer mainVersion;

    private String definitionId;

    private Integer autoCompleteFirstNode;

}
